import * as React from 'react'
import styled from 'styled-components'
import { Button, Text } from 'native-base'

const SignInButtonStyles = styled(Button) `
    background-color: rgba(255, 255, 255, 0.2);
    width: 294px;
    height: auto;
    justify-content: center;
    align-self: center;
`

const SignInText = styled(Text) `
    color: white;
    font-size: 24px;
    padding-top: 20;
    padding-bottom: 20;
`

const PlayTrackButtonStyles = styled(Button)`
    background-color: rgba(255, 255, 255, 0.2);
    width: 294px;
    height: auto;
    justify-content: center;
    align-self: center;
`

const PlayTrackText = styled(Text)`
    color: white;
    font-size: 24px;
    padding-top: 20;
    padding-bottom: 20;
`

interface SignInButtonProps {
    addedStyle: object,
}

const SignInButton = (props: SignInButtonProps) => {
    const { addedStyle } = props
    return (
        <SignInButtonStyles transparent style={addedStyle}>
            <SignInText>
                Sign In
            </SignInText>
        </SignInButtonStyles>
    )
}

interface PlayTrackButtonProps {
    addedStyle: object,
    onPress: () => void,
}

const PlayTrackButton = (props: PlayTrackButtonProps) => {
    const { addedStyle, onPress } = props
    return (
        <PlayTrackButtonStyles
           style={addedStyle}
           onPress={onPress}
        >
            <PlayTrackText>
                Play Track
            </PlayTrackText>
        </PlayTrackButtonStyles>
    )}

export {
    SignInButton,
    PlayTrackButton,
}