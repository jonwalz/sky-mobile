import * as React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { StackNavigator, SafeAreaView } from 'react-navigation'
import styled from 'styled-components'

const SideBarStyles = styled(View)`
    flex: 1;
    flex-direction: column;
    background-color: white;
    padding-top: 20px;
`

export default class Sidebar extends React.Component<{}> {
    render() {
        return (
            <SideBarStyles>
                <Text>SideBar</Text>
            </SideBarStyles>
        )
    }
}