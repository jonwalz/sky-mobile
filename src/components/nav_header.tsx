
import * as React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { StackNavigator, SafeAreaView } from 'react-navigation'
import { Header, Left, Button, Icon } from 'native-base'
import styled from 'styled-components'

const BlueHeader = styled(Header) `
	background-color: #265669;
    flex: 1;
    width: 100%;
`

export const NavHeader = () => {
    return (
        <Button transparent>
            <Icon name="menu" style={{ color: 'white' }} />
        </Button>
    )
}