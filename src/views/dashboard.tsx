import * as React from 'react'
import { TouchableOpacity, StyleSheet, Text, View, Image, SafeAreaView } from 'react-native'
import * as ReactNavigation from 'react-navigation'
import styled from 'styled-components'
import NativeBase, { Content, Input, Item, Container, Header, Left, Button, Icon } from 'native-base'
import { NavHeader } from '../components/nav_header'
import { SignInButton, PlayTrackButton } from '../components/buttons'
const WhiteMic = require('../assets/white-mic.png')

const StyledView = styled.View`
    background-color: #265669;
    flex: 1;
    align-items: center;
`;

const StyledText = styled.Text`
	color: white;
`

interface DashboardProps {
    navigation: ReactNavigation.navigate,
}

export class Dashboard extends React.Component<DashboardProps> {
	static navigationOptions = {
		headerRight: <NavHeader />
	}
    render() {
        return (
                <StyledView>
                        <View style={styles.micImageContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Listen')}>
                                <Image source={WhiteMic} />
                            </TouchableOpacity>
                        </View>
                    <SignInButton addedStyle={styles.signInButton}/>
                    <PlayTrackButton
                        addedStyle={styles.playTrackButton}
                        onPress={() => this.props.navigation.navigate('TrackList')}
                    />
                </StyledView>
        )
    }
}

const styles = StyleSheet.create({
	signInButton: {
        marginTop: 50,
    },
    playTrackButton: {
        marginTop: 50,
    },
	micImageContainer: {
        marginTop: 50,
	}
})
