import * as React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { StackNavigator, SafeAreaView } from 'react-navigation'
import styled from 'styled-components'
import NativeBase, { Drawer } from 'native-base'
import { Dashboard } from './src/views/dashboard'
import { TrackList } from './src/views/track_list'
import { Listen } from './src/views/listen'
import Sidebar from './src/components/sidebar'
const WhiteMic = require('./src/assets/white-mic.png')

const RootStack = StackNavigator({
		Dashboard: {
			screen: Dashboard,
		},
		TrackList: {
			screen: TrackList,
        },
        Listen: {
            screen: Listen,
        }
	},
	{
		initialRouteName: 'Dashboard',
		navigationOptions: {
			headerStyle: {
			  backgroundColor: '#265669',
			  paddingTop: 5,
			  paddingBottom: 5,
			}
		}
	}
)

interface AppProps {}

interface AppState {
	isDrawerOpen: boolean
}

export default class App extends React.Component<AppProps, AppState> {
	private drawer!: NativeBase.Drawer | null
	constructor(props: AppProps) {
		super(props)

		this.closeDrawer = this.closeDrawer.bind(this)

		this.state = {
			isDrawerOpen: false,
		}
	}

	closeDrawer() {
		this.setState({
			isDrawerOpen: true
		})
	}

	openDrawer() {
		this.setState({
			isDrawerOpen: true,
		})
	}

	render() {
		const { isDrawerOpen } = this.state
		return (
			<Drawer
				ref={(ref) => { this.drawer = ref }}
				content={<Sidebar />}
				open={isDrawerOpen}
			>
				<RootStack/>
			</Drawer>
	)}
}
